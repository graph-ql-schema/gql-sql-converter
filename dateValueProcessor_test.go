package gql_sql_converter

import (
	"reflect"
	"testing"
	"time"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Тестирование доступности процессора
func Test_dateValueProcessor_IsAvailable(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.DateTime,
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.String,
					Name: "field_2",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на валидном поле",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				value:  nil,
				field:  "field_1",
			},
			want: true,
		},
		{
			name: "Тестирование на не валидном поле",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				value:  nil,
				field:  "field_2",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := dateValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			if got := d.IsAvailable(tt.args.object, tt.args.field, tt.args.value); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации в базовый тип
func Test_dateValueProcessor_ToBaseType(t *testing.T) {
	date, _ := time.Parse(time.RFC3339, "2014-11-12T11:45:26.371Z")

	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование на не валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: false,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: "2014-11-12T11:45:26.371Z",
			},
			want:    date,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := dateValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			got, err := d.ToBaseType(tt.args.object, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToBaseType() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToBaseType() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации в SQL like значение
func Test_dateValueProcessor_ToSQLValue(t *testing.T) {
	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование на не валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: false,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: "2014-11-12T11:45:26.371Z",
			},
			want:    "'2014-11-12T11:45:26.371Z'",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := dateValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			got, err := d.ToSQLValue(tt.args.object, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQLValue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToSQLValue() got = %v, want %v", got, tt.want)
			}
		})
	}
}
