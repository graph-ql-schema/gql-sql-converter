package gql_sql_converter

import (
	gql_root_type_getter "bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
	"reflect"
	"testing"
)

// Тестирование доступности процессора
func Test_idValueProcessor_IsAvailable(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Float,
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.ID,
					Name: "field_2",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не валидном поле",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				value:  nil,
				field:  "field_1",
			},
			want: false,
		},
		{
			name: "Тестирование на валидном поле",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				value:  nil,
				field:  "field_2",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := idValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			if got := i.IsAvailable(tt.args.object, tt.args.field, tt.args.value); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации в базовый тип
func Test_idValueProcessor_ToBaseType(t *testing.T) {
	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование на не валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: complex(1, 1),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: "test",
			},
			want:    "test",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := idValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			got, err := i.ToBaseType(tt.args.object, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToBaseType() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToBaseType() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации в SQL like значение
func Test_idValueProcessor_ToSQLValue(t *testing.T) {
	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование на не валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: complex(1, 1),
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: "test",
			},
			want:    "test",
			wantErr: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: 11122233311233322,
			},
			want:    "11122233311233322",
			wantErr: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: int64(1112223331123332231),
			},
			want:    "1112223331123332231",
			wantErr: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: float64(1112223331123),
			},
			want:    "1112223331123",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := idValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			got, err := i.ToSQLValue(tt.args.object, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQLValue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToSQLValue() got = %v, want %v", got, tt.want)
			}
		})
	}
}
