package gql_sql_converter

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Конвертер значений для парсера параметров
type graphQlSqlConverter struct {
	processors []graphQlSqlConverterProcessorInterface
}

// Конвертация в базовый тип, например в строку или число
func (g graphQlSqlConverter) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	if nil == object {
		return nil, fmt.Errorf(`passed nil object to value converter`)
	}

	if _, ok := object.Fields()[field]; !ok {
		return nil, fmt.Errorf(`field '%v' is not found in passed structure`, field)
	}

	for _, processor := range g.processors {
		if processor.IsAvailable(object, field, value) {
			return processor.ToBaseType(object, field, value)
		}
	}

	return nil, fmt.Errorf(`no available processor for field '%v'`, field)
}

// Конвертация в SQL like значение
func (g graphQlSqlConverter) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	if nil == object {
		return "", fmt.Errorf(`passed nil object to value converter`)
	}

	if _, ok := object.Fields()[field]; !ok {
		return "", fmt.Errorf(`field '%v' is not found in passed structure`, field)
	}

	for _, processor := range g.processors {
		if processor.IsAvailable(object, field, value) {
			return processor.ToSQLValue(object, field, value)
		}
	}

	return "", fmt.Errorf(`no available processor for field '%v'`, field)
}

// Фабрика конвертера
func NewGraphQlSqlConverter() GraphQlSqlConverterInterface {
	return &graphQlSqlConverter{
		processors: []graphQlSqlConverterProcessorInterface{
			newEnumValueProcessor(),
			newBoolValueProcessor(),
			newDateValueProcessor(),
			newFloatValueProcessor(),
			newIntValueProcessor(),
			newStringValueProcessor(),
			newIdValueProcessor(),
		},
	}
}
