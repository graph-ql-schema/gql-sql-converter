package gql_sql_converter

import (
	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"fmt"
	"github.com/graphql-go/graphql"
	"strconv"
	"strings"
	"time"
)

// Процессор значений ID
type idValueProcessor struct {
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Тестирование процессора на доступность
func (i idValueProcessor) IsAvailable(object *graphql.Object, field string, value interface{}) bool {
	fieldData := object.Fields()[field]
	fieldType := i.typeGetter.GetRootType(fieldData.Type)

	return fieldType == graphql.ID
}

// Конвертация в базовый тип, например в строку или число
func (i idValueProcessor) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	switch val := value.(type) {
	case int:
		if val == 0 {
			return nil, nil
		}

		return strconv.Itoa(val), nil
	case int8:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case int16:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case int32:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case int64:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case uint:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case uint8:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case uint16:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case uint32:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case uint64:
		if val == 0 {
			return nil, nil
		}

		return fmt.Sprintf(`%d`, val), nil
	case float64:
		if val == 0 {
			return nil, nil
		}

		if val == float64(int64(val)) {
			return fmt.Sprintf(`%d`, int64(val)), nil
		}

		return fmt.Sprintf(`%f`, val), nil
	case float32:
		if val == 0 {
			return nil, nil
		}

		if val == float32(int64(val)) {
			return fmt.Sprintf(`%d`, int64(val)), nil
		}

		return fmt.Sprintf(`%f`, val), nil
	case time.Time:
		return val.Format(time.RFC3339), nil
	case bool:
		return fmt.Sprintf(`%v`, val), nil
	case string:
		if val == `` || strings.ToLower(val) == `nil` {
			return nil, nil
		}

		return val, nil
	default:
		return nil, fmt.Errorf(`passed value is not of type ID, value '%v'`, value)
	}
}

// Конвертация в SQL like значение
func (i idValueProcessor) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	val, err := i.ToBaseType(object, field, value)
	if nil != err {
		return "", err
	}

	if nil == val {
		return "null", nil
	}

	return fmt.Sprintf(`%v`, val), nil
}

// Фабрика процессора
func newIdValueProcessor() graphQlSqlConverterProcessorInterface {
	return &idValueProcessor{
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
