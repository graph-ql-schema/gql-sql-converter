package gql_sql_converter

import (
	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"encoding/json"
	"fmt"
	"github.com/graphql-go/graphql"
	"time"
)

// Обработка значений типа Enum
type enumValueProcessor struct {
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Тестирование процессора на доступность
func (e enumValueProcessor) IsAvailable(object *graphql.Object, field string, value interface{}) bool {
	fieldData := object.Fields()[field]
	fieldType := e.typeGetter.GetRootType(fieldData.Type)

	if _, ok := fieldType.(*graphql.Enum); ok {
		return true
	}

	return false
}

// Конвертация в базовый тип, например в строку или число
func (e enumValueProcessor) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	if nil == value {
		return nil, nil
	}

	fieldData := object.Fields()[field]
	fieldType := e.typeGetter.GetRootType(fieldData.Type)
	parsedField := fieldType.(*graphql.Enum)

	stringPassedVal, _ := json.Marshal(value)
	for _, val := range parsedField.Values() {
		stringVal, _ := json.Marshal(val.Value)
		if string(stringPassedVal) == string(stringVal) {
			return val.Value, nil
		}
	}

	return nil, nil
}

// Конвертация в SQL like значение
func (e enumValueProcessor) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	baseValue, err := e.ToBaseType(object, field, value)
	if nil != err {
		return "", err
	}

	if nil == baseValue {
		return "null", nil
	}

	switch val := baseValue.(type) {
	case string:
		return fmt.Sprintf(`'%v'`, val), nil
	case int:
		return fmt.Sprintf(`%v`, val), nil
	case int8:
		return fmt.Sprintf(`%v`, val), nil
	case int16:
		return fmt.Sprintf(`%v`, val), nil
	case int32:
		return fmt.Sprintf(`%v`, val), nil
	case int64:
		return fmt.Sprintf(`%v`, val), nil
	case uint:
		return fmt.Sprintf(`%v`, val), nil
	case uint8:
		return fmt.Sprintf(`%v`, val), nil
	case uint16:
		return fmt.Sprintf(`%v`, val), nil
	case uint32:
		return fmt.Sprintf(`%v`, val), nil
	case uint64:
		return fmt.Sprintf(`%v`, val), nil
	case float32:
		return fmt.Sprintf(`%v`, val), nil
	case float64:
		return fmt.Sprintf(`%v`, val), nil
	case bool:
		if val {
			return "true", nil
		}

		return "false", nil
	case time.Time:
		return fmt.Sprintf(`'%v'`, val.Format(time.RFC3339Nano)), nil
	default:
		return ``, fmt.Errorf(`failed to convert field value. you should pass string/date/number/bool, another types is not supported`)
	}
}

// Конструктор процессора
func newEnumValueProcessor() graphQlSqlConverterProcessorInterface {
	return &enumValueProcessor{
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
