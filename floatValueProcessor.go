package gql_sql_converter

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Процессор дробных значений
type floatValueProcessor struct {
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Тестирование процессора на доступность
func (f floatValueProcessor) IsAvailable(object *graphql.Object, field string, value interface{}) bool {
	fieldData := object.Fields()[field]
	fieldType := f.typeGetter.GetRootType(fieldData.Type)

	return fieldType == graphql.Float
}

// Конвертация в базовый тип, например в строку или число
func (f floatValueProcessor) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	switch val := value.(type) {
	case uint:
		return float64(val), nil
	case uint8:
		return float64(val), nil
	case uint16:
		return float64(val), nil
	case uint32:
		return float64(val), nil
	case uint64:
		return float64(val), nil
	case int:
		return float64(val), nil
	case int8:
		return float64(val), nil
	case int16:
		return float64(val), nil
	case int32:
		return float64(val), nil
	case int64:
		return float64(val), nil
	case float64:
		return val, nil
	case float32:
		return val, nil
	case string:
		if strings.ToLower(val) == "nil" {
			return nil, nil
		}

		res, err := strconv.ParseFloat(val, 0)
		if nil != err {
			return nil, err
		}

		return res, nil
	default:
		return nil, fmt.Errorf(`passed value is not of type float, value '%v'`, value)
	}
}

// Конвертация в SQL like значение
func (f floatValueProcessor) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	val, err := f.ToBaseType(object, field, value)
	if nil != err {
		return "", err
	}

	if nil == val {
		return "null", nil
	}

	return fmt.Sprintf(`%v`, val), nil
}

// Фабрика процессора
func newFloatValueProcessor() graphQlSqlConverterProcessorInterface {
	return &floatValueProcessor{
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
