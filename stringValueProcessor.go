package gql_sql_converter

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Процессор строковых значений
type stringValueProcessor struct {
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Тестирование процессора на доступность
func (s stringValueProcessor) IsAvailable(object *graphql.Object, field string, value interface{}) bool {
	fieldData := object.Fields()[field]
	fieldType := s.typeGetter.GetRootType(fieldData.Type)

	return fieldType == graphql.String
}

// Конвертация в базовый тип, например в строку или число
func (s stringValueProcessor) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	switch val := value.(type) {
	case int:
		return fmt.Sprintf(`%d`, val), nil
	case int8:
		return fmt.Sprintf(`%d`, val), nil
	case int16:
		return fmt.Sprintf(`%d`, val), nil
	case int32:
		return fmt.Sprintf(`%d`, val), nil
	case int64:
		return fmt.Sprintf(`%d`, val), nil
	case uint:
		return fmt.Sprintf(`%d`, val), nil
	case uint8:
		return fmt.Sprintf(`%d`, val), nil
	case uint16:
		return fmt.Sprintf(`%d`, val), nil
	case uint32:
		return fmt.Sprintf(`%d`, val), nil
	case uint64:
		return fmt.Sprintf(`%d`, val), nil
	case float64:
		if val == float64(int64(val)) {
			return fmt.Sprintf(`%d`, int64(val)), nil
		}

		return fmt.Sprintf(`%f`, val), nil
	case float32:
		if val == float32(int64(val)) {
			return fmt.Sprintf(`%d`, int64(val)), nil
		}

		return fmt.Sprintf(`%f`, val), nil
	case time.Time:
		return val.Format(time.RFC3339), nil
	case bool:
		return fmt.Sprintf(`%v`, val), nil
	case string:
		if strings.ToLower(val) == "nil" {
			return nil, nil
		}

		return val, nil
	default:
		return nil, fmt.Errorf(`passed value is not of type string, value '%v'`, value)
	}
}

// Конвертация в SQL like значение
func (s stringValueProcessor) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	val, err := s.ToBaseType(object, field, value)
	if nil != err {
		return "", err
	}

	if nil == val {
		return "null", nil
	}

	return fmt.Sprintf(`%v`, val), nil
}

// Фабрика процессора
func newStringValueProcessor() graphQlSqlConverterProcessorInterface {
	return &stringValueProcessor{
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
