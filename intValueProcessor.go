package gql_sql_converter

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Процессор целых чисел
type intValueProcessor struct {
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Тестирование процессора на доступность
func (i intValueProcessor) IsAvailable(object *graphql.Object, field string, value interface{}) bool {
	fieldData := object.Fields()[field]
	fieldType := i.typeGetter.GetRootType(fieldData.Type)

	return fieldType == graphql.Int
}

// Конвертация в базовый тип, например в строку или число
func (i intValueProcessor) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	switch val := value.(type) {
	case uint:
		return val, nil
	case uint8:
		return val, nil
	case uint16:
		return val, nil
	case uint32:
		return val, nil
	case uint64:
		return val, nil
	case int:
		return val, nil
	case int8:
		return val, nil
	case int16:
		return val, nil
	case int32:
		return val, nil
	case int64:
		return val, nil
	case float64:
		return int(val), nil
	case float32:
		return int(val), nil
	case string:
		if strings.ToLower(val) == "nil" {
			return nil, nil
		}

		res, err := strconv.ParseInt(val, 0, 0)
		if nil != err {
			return nil, err
		}

		return res, nil
	default:
		return nil, fmt.Errorf(`passed value is not of type int, value '%v'`, value)
	}
}

// Конвертация в SQL like значение
func (i intValueProcessor) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	val, err := i.ToBaseType(object, field, value)
	if nil != err {
		return "", err
	}

	if nil == val {
		return "null", nil
	}

	return fmt.Sprintf(`%v`, val), nil
}

// Фабрика процессора
func newIntValueProcessor() graphQlSqlConverterProcessorInterface {
	return &intValueProcessor{
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
