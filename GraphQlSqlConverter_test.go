package gql_sql_converter

import (
	"reflect"
	"testing"

	"github.com/graphql-go/graphql"
)

// Тестирование конвертации в базовый тип
func Test_graphQlSqlConverter_ToBaseType(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		processors []graphQlSqlConverterProcessorInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование без процессоров",
			fields: fields{
				processors: nil,
			},
			args: args{
				object: objectConfig,
				field:  "field_1",
				value:  nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование с пустим объектом",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: true,
					},
				},
			},
			args: args{
				object: nil,
				field:  "field_1",
				value:  nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование без валидных процессоров",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: false,
					},
				},
			},
			args: args{
				object: objectConfig,
				field:  "field_1",
				value:  nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на отсутствующем поле",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: true,
						ToBaseTypeResult:  "test",
						ToBaseTypeError:   false,
					},
				},
			},
			args: args{
				object: objectConfig,
				field:  "field",
				value:  nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование с ошибкой обработки у процессора",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: true,
						ToBaseTypeResult:  "test",
						ToBaseTypeError:   true,
					},
				},
			},
			args: args{
				object: objectConfig,
				field:  "field_1",
				value:  nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование с корректной обработкой у процессора",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: true,
						ToBaseTypeResult:  "test",
						ToBaseTypeError:   false,
					},
				},
			},
			args: args{
				object: objectConfig,
				field:  "field_1",
				value:  nil,
			},
			want:    "test",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := graphQlSqlConverter{
				processors: tt.fields.processors,
			}
			got, err := v.ToBaseType(tt.args.object, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToBaseType() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToBaseType() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации в SQL like значение
func Test_graphQlSqlConverter_ToSQLValue(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		processors []graphQlSqlConverterProcessorInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование без процессоров",
			fields: fields{
				processors: nil,
			},
			args: args{
				object: objectConfig,
				field:  "field_1",
				value:  nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование с пустим объектом",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: true,
					},
				},
			},
			args: args{
				object: nil,
				field:  "field_1",
				value:  nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование без валидных процессоров",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: false,
					},
				},
			},
			args: args{
				object: objectConfig,
				field:  "field_1",
				value:  nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование на отсутствующем поле",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: true,
						ToSQLValueResult:  "test",
						ToSQLValueError:   false,
					},
				},
			},
			args: args{
				object: objectConfig,
				field:  "field",
				value:  nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование с ошибкой обработки у процессора",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: true,
						ToSQLValueResult:  "test",
						ToSQLValueError:   true,
					},
				},
			},
			args: args{
				object: objectConfig,
				field:  "field_1",
				value:  nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование с корректной обработкой у процессора",
			fields: fields{
				processors: []graphQlSqlConverterProcessorInterface{
					&graphQlSqlConverterProcessorMock{
						IsAvailableResult: true,
						ToSQLValueResult:  "test",
						ToSQLValueError:   false,
					},
				},
			},
			args: args{
				object: objectConfig,
				field:  "field_1",
				value:  nil,
			},
			want:    "test",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := graphQlSqlConverter{
				processors: tt.fields.processors,
			}
			got, err := v.ToSQLValue(tt.args.object, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQLValue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToSQLValue() got = %v, want %v", got, tt.want)
			}
		})
	}
}
