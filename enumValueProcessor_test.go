package gql_sql_converter

import (
	gql_root_type_getter "bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
	"reflect"
	"testing"
)

// Тестирование доступности процессора
func Test_enumValueProcessor_IsAvailable(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Float,
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.NewEnum(graphql.EnumConfig{
						Name: "Test",
						Values: graphql.EnumValueConfigMap{
							"name1": {Value: "1"},
						},
						Description: "Test",
					}),
					Name: "field_2",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не валидном поле",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				value:  nil,
				field:  "field_1",
			},
			want: false,
		},
		{
			name: "Тестирование на валидном поле",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				value:  nil,
				field:  "field_2",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := enumValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			if got := e.IsAvailable(tt.args.object, tt.args.field, tt.args.value); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации в базовый тип
func Test_enumValueProcessor_ToBaseType(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Float,
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.NewEnum(graphql.EnumConfig{
						Name: "Test",
						Values: graphql.EnumValueConfigMap{
							"name1": {Value: int64(1)},
						},
						Description: "Test",
					}),
					Name: "field_2",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование конвертации в базовый тип",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				field:  "field_2",
				value:  1,
			},
			want:    int64(1),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := enumValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			got, err := e.ToBaseType(tt.args.object, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToBaseType() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToBaseType() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации в SQL значение
func Test_enumValueProcessor_ToSQLValue(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Float,
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.NewEnum(graphql.EnumConfig{
						Name: "Test",
						Values: graphql.EnumValueConfigMap{
							"name1": {Value: int64(1)},
						},
						Description: "Test",
					}),
					Name: "field_2",
				},
				"field_3": &graphql.Field{
					Type: graphql.NewEnum(graphql.EnumConfig{
						Name: "Test3",
						Values: graphql.EnumValueConfigMap{
							"name1": {Value: "1"},
						},
						Description: "Test",
					}),
					Name: "field_3",
				},
				"field_4": &graphql.Field{
					Type: graphql.NewEnum(graphql.EnumConfig{
						Name: "Test4",
						Values: graphql.EnumValueConfigMap{
							"name1": {Value: true},
						},
						Description: "Test",
					}),
					Name: "field_4",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
		field  string
		value  interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование числа",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				field:  "field_2",
				value:  1,
			},
			want:    `1`,
			wantErr: false,
		},
		{
			name: "Тестирование строки",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				field:  "field_3",
				value:  `1`,
			},
			want:    `'1'`,
			wantErr: false,
		},
		{
			name: "Тестирование логического значения",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				field:  "field_4",
				value:  true,
			},
			want:    `true`,
			wantErr: false,
		},
		{
			name: "Тестирование nil",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				object: objectConfig,
				field:  "field_4",
				value:  nil,
			},
			want:    `null`,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := enumValueProcessor{
				typeGetter: tt.fields.typeGetter,
			}
			got, err := e.ToSQLValue(tt.args.object, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQLValue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToSQLValue() got = %v, want %v", got, tt.want)
			}
		})
	}
}
