module bitbucket.org/graph-ql-schema/gql-sql-converter

go 1.14

require (
	bitbucket.org/graph-ql-schema/gql-root-type-getter v1.3.1
	github.com/graphql-go/graphql v0.7.9
)
