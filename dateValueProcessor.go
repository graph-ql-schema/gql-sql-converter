package gql_sql_converter

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Процессор значений даты и времени
type dateValueProcessor struct {
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Тестирование процессора на доступность
func (d dateValueProcessor) IsAvailable(object *graphql.Object, field string, value interface{}) bool {
	fieldData := object.Fields()[field]
	fieldType := d.typeGetter.GetRootType(fieldData.Type)

	return fieldType == graphql.DateTime
}

// Конвертация в базовый тип, например в строку или число
func (d dateValueProcessor) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	switch val := value.(type) {
	case int:
		return time.Unix(int64(val), 0), nil
	case int8:
		return time.Unix(int64(val), 0), nil
	case int16:
		return time.Unix(int64(val), 0), nil
	case int32:
		return time.Unix(int64(val), 0), nil
	case int64:
		return time.Unix(val, 0), nil
	case uint:
		return time.Unix(int64(val), 0), nil
	case uint8:
		return time.Unix(int64(val), 0), nil
	case uint16:
		return time.Unix(int64(val), 0), nil
	case uint32:
		return time.Unix(int64(val), 0), nil
	case uint64:
		return time.Unix(int64(val), 0), nil
	case float32:
		return time.Unix(int64(val), 0), nil
	case float64:
		return time.Unix(int64(val), 0), nil
	case time.Time:
		return val, nil
	case string:
		if strings.ToLower(val) == "nil" {
			return nil, nil
		}

		dTime, err := time.Parse(time.RFC3339Nano, val)
		if nil != err {
			return nil, fmt.Errorf(`can't parse datetime for field: %v'`, err.Error())
		}

		return dTime, nil
	default:
		return nil, fmt.Errorf(`passed value is not of type datetime, value '%v'`, value)
	}
}

// Конвертация в SQL like значение
func (d dateValueProcessor) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	val, err := d.ToBaseType(object, field, value)
	if nil != err {
		return "", err
	}

	if nil == val {
		return "null", nil
	}

	parsedVal, _ := val.(time.Time)

	return fmt.Sprintf(`'%v'`, parsedVal.Format(time.RFC3339Nano)), nil
}

// Фабрика процессора
func newDateValueProcessor() graphQlSqlConverterProcessorInterface {
	return &dateValueProcessor{
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
