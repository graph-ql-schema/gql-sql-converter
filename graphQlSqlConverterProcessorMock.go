package gql_sql_converter

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type graphQlSqlConverterProcessorMock struct {
	IsAvailableResult bool
	ToBaseTypeResult  interface{}
	ToBaseTypeError   bool
	ToSQLValueResult  string
	ToSQLValueError   bool
}

// Тетсирование процессора на доступность
func (v graphQlSqlConverterProcessorMock) IsAvailable(*graphql.Object, string, interface{}) bool {
	return v.IsAvailableResult
}

// Конвертация в базовый тип, например в строку или число
func (v graphQlSqlConverterProcessorMock) ToBaseType(*graphql.Object, string, interface{}) (interface{}, error) {
	if v.ToBaseTypeError {
		return nil, fmt.Errorf(`test`)
	}

	return v.ToBaseTypeResult, nil
}

// Конвертация в SQL like значение
func (v graphQlSqlConverterProcessorMock) ToSQLValue(*graphql.Object, string, interface{}) (string, error) {
	if v.ToSQLValueError {
		return "", fmt.Errorf(`test`)
	}

	return v.ToSQLValueResult, nil
}
