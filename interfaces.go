package gql_sql_converter

import "github.com/graphql-go/graphql"

// Интерфейс конвертера значения
type GraphQlSqlConverterInterface interface {
	// Конвертация в базовый тип, например в строку или число
	ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error)

	// Конвертация в SQL like значение
	ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error)
}

// Процессор для обработки конвертации значения
type graphQlSqlConverterProcessorInterface interface {
	// Тестирование процессора на доступность
	IsAvailable(object *graphql.Object, field string, value interface{}) bool

	// Конвертация в базовый тип, например в строку или число
	ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error)

	// Конвертация в SQL like значение
	ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error)
}
