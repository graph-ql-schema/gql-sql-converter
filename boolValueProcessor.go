package gql_sql_converter

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Процессор логических значений
type boolValueProcessor struct {
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Тестирование процессора на доступность
func (b boolValueProcessor) IsAvailable(object *graphql.Object, field string, value interface{}) bool {
	fieldData := object.Fields()[field]
	fieldType := b.typeGetter.GetRootType(fieldData.Type)

	return fieldType == graphql.Boolean
}

// Конвертация в базовый тип, например в строку или число
func (b boolValueProcessor) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	switch val := value.(type) {
	case int:
		return 1 == val, nil
	case int8:
		return 1 == val, nil
	case int16:
		return 1 == val, nil
	case int32:
		return 1 == val, nil
	case int64:
		return 1 == val, nil
	case uint:
		return 1 == val, nil
	case uint8:
		return 1 == val, nil
	case uint16:
		return 1 == val, nil
	case uint32:
		return 1 == val, nil
	case uint64:
		return 1 == val, nil
	case float32:
		return 1 == val, nil
	case float64:
		return 1 == val, nil
	case bool:
		return val, nil
	case string:
		val = strings.ToLower(val)
		if val == "nil" {
			return nil, nil
		}

		if "true" != val && "false" != val {
			return nil, fmt.Errorf(`failed to parse boolean value from string '%v'`, val)
		}

		return "true" == val, nil
	default:
		return nil, fmt.Errorf(`passed value is not of type bool, value '%v'`, value)
	}
}

// Конвертация в SQL like значение
func (b boolValueProcessor) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	val, err := b.ToBaseType(object, field, value)
	if nil != err {
		return "", err
	}

	if nil == val {
		return "null", nil
	}

	return fmt.Sprintf(`%v`, val), nil
}

// Фабрика процессора
func newBoolValueProcessor() graphQlSqlConverterProcessorInterface {
	return &boolValueProcessor{
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
