package gql_sql_converter

import "github.com/graphql-go/graphql"

// Сервис получения корневого типа GraphQL по переданному.
type graphQlRootTypeGetterMock struct {
	IsCalled         bool
	IsNotNullReturn  bool
	IsListReturn     bool
	IsNullableReturn bool
}

// Проверяет, что переданный тип является Nullable
func (g *graphQlRootTypeGetterMock) IsNullable(gType graphql.Type) bool {
	return g.IsNullableReturn
}

// Проверяет, что переданный тип является List
func (g *graphQlRootTypeGetterMock) IsList(gType graphql.Type) bool {
	return g.IsListReturn
}

// Проверяет, что переданный тип не является NotNull
func (g *graphQlRootTypeGetterMock) IsNotNull(graphql.Type) bool {
	return g.IsNotNullReturn
}

// Получение корневого типа поля, которое обернут в NotNull или в List
func (g *graphQlRootTypeGetterMock) GetRootType(gType graphql.Type) graphql.Type {
	g.IsCalled = true

	return gType
}
